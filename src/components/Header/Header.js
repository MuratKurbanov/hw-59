import React from 'react';

const Header = (props) => {
    return (
        <div>
            <input type="text"
                   placeholder="Add Movie"
                   onChange={(e) => props.value(e.target.value)}
            />
            <button onClick={props.added}>Add</button>
        </div>
    );
};

export default Header;