import React, { Component} from 'react';

class Movie extends Component {

    render() {
    return (
        <div>
            <input type="text" value={this.props.name} onChange={this.props.changeMovie}/>
            <button onClick={this.props.remove}>X</button>
        </div>
    );
    };
};

export default Movie;