import React, { Component } from 'react';

import Header from './components/Header/Header';
import Movie from "./components/Movie/Movie";

class App extends Component {

  state = {
    value: '',
    movie: []
  };

  headerInput = (value) => {
    this.setState({value})
  };

  addMovie = () => {
    const movie = this.state.movie;
    const newMovie = {name: this.state.value};
    movie.push(newMovie);
    this.setState({movie})
  };

  remove = (index) => {
    let movies = this.state.movie;
    movies.splice(index, 1);
    this.setState({
      movie: movies
    })
  };

  changeMovie = (event, id) => {
    let movie = this.state.movie;
    movie[id].name = event;
    this.setState({movie})
  };

  render() {
    return (
      <div className="App">
        <Header value={this.headerInput}
                added={this.addMovie}
        />
        {this.state.movie.map((movie, index) => {
          return (
              <Movie key={index}
                     name={movie.name}
                     remove={() => this.remove(index)}
                     changeMovie={(event) => this.changeMovie(event.target.value, index)}
              />
          )
        })}
      </div>
    );
  }
}

export default App;
